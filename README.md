# tdcx_prediction

The repository is the code localizing and classifying the types of fruits displayed. The prediction is using the retinanet for detecting the object. The model was trained using the tensorflow model zoo. To setup the training enviroment and traing code use the below link

[Tensorflow object detection setup](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/)

[Tensorflow model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md)


# Getting Started

To get started with the prediction code you need to open a conda or other python virtual environment and install the dependencies from the requirement.txt file

Go to the base folder and execute the following command

```
pip install -r "requirements.txt"
```

# Run prediction command 

To run the prediction command you need to use the following command


```
python prediction.py --model_dir <model path> --path <image path>
```

![sample prediction](assets/image1.jpg)



