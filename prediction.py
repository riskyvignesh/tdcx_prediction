"""
Object Detection (On Image) From TF2 Saved Model
=====================================
"""

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
import pathlib
import tensorflow as tf
import cv2
import argparse
# import pyplot as plt
# import tensorflow as tf
# from google.colab.patches import cv2_imshow

# Enable GPU dynamic memory allocation
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

# import argparse, os

parser = argparse.ArgumentParser()
parser.add_argument('--path', help= 'paste path to biog.txt file')
parser.add_argument('--model_dir', help= 'paste path to biog.txt file')
args = parser.parse_args()
# print("path:", args.path)

# PROVIDE PATH TO IMAGE DIRECTORY
IMAGE_PATHS = args.path
# '/home/jayanthan/Desktop/tdcx/training/training_demo/images/train/mixed_4.jpg'


# PROVIDE PATH TO MODEL DIRECTORY
PATH_TO_MODEL_DIR = args.model_dir

# PROVIDE PATH TO LABEL MAP
PATH_TO_LABELS = '/home/jayanthan/Desktop/tdcx/training/training_demo/annotations/label_map.pbtxt'

# PROVIDE THE MINIMUM CONFIDENCE THRESHOLD
MIN_CONF_THRESH = float(0.60)

# LOAD THE MODEL

import time
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils

PATH_TO_SAVED_MODEL = PATH_TO_MODEL_DIR + "/saved_model"

print('Loading model...', end='')
start_time = time.time()

# LOAD SAVED MODEL AND BUILD DETECTION FUNCTION
detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)

end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))

# LOAD LABEL MAP DATA FOR PLOTTING

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                    use_display_name=True)

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.
    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.
    Args:
      path: the file path to the image
    Returns:
      uint8 numpy array with shape (img_height, img_width, 3)
    """
    return np.array(Image.open(path))




print('Running inference for {}... '.format(IMAGE_PATHS), end='')

image = cv2.imread(IMAGE_PATHS)
image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image_expanded = np.expand_dims(image_rgb, axis=0)

# The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
input_tensor = tf.convert_to_tensor(image)
# The model expects a batch of images, so add an axis with `tf.newaxis`.
input_tensor = input_tensor[tf.newaxis, ...]

# input_tensor = np.expand_dims(image_np, 0)
detections = detect_fn(input_tensor)

# All outputs are batches tensors.
# Convert to numpy arrays, and take index [0] to remove the batch dimension.
# We're only interested in the first num_detections.
num_detections = int(detections.pop('num_detections'))
detections = {key: value[0, :num_detections].numpy()
               for key, value in detections.items()}
detections['num_detections'] = num_detections

# detection_classes should be ints.
detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

image_with_detections = image.copy()


# SET MIN_SCORE_THRESH BASED ON YOU MINIMUM THRESHOLD FOR DETECTIONS
viz_utils.visualize_boxes_and_labels_on_image_array(
      image_with_detections,
      detections['detection_boxes'],
      detections['detection_classes'],
      detections['detection_scores'],
      category_index,
      use_normalized_coordinates=True,
      max_boxes_to_draw=200,
      min_score_thresh=0.5,
      agnostic_mode=False)



nms = tf.image.non_max_suppression(
    detections['detection_boxes'], detections['detection_scores'], 5, iou_threshold=0.5,
    score_threshold=float('-inf'), name=None
)

nms=list(nms.numpy())
final_boxes = [detections['detection_boxes'][indi] for indi in nms ]
final_class = [detections['detection_classes'][indi] for indi in nms ]


# DISPLAYS OUTPUT IMAGE

for i in range(0, len(final_boxes)):
  colors = [(255,0,0), (0,255,0),(0,0,255)]
  start_point = int(final_boxes[i][0]*image_with_detections.shape[1]),int(final_boxes[i][1]*image_with_detections.shape[0])
  end_point = int(final_boxes[i][2]*image_with_detections.shape[1]),int(final_boxes[i][3]*image_with_detections.shape[0])
  # print(start_point, end_point)
  cv2.rectangle(image_with_detections,start_point,
            end_point,colors[final_class[i]-1],4)


cv2.imshow("image",image_with_detections)
cv2.waitKey(0)
